import java.io.*;
import java.util.Scanner;
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.geometry.*;
import javafx.event.EventHandler;
import javafx.event.ActionEvent;


public class Main extends Application {
    
    public static void main(String args[]) {
        launch(args);
    }
    

    public void start(Stage primaryStage) {
	int songTimer = 0;
	boolean paused = true;
	Song currSongPlayed = null;
	
	Dafsa dafsa = loadData();
	
	Playlist playlist = new Playlist();

	HBox hb0 = new HBox();
	HBox hb1 = new HBox();
	hb1.setSpacing(100);
	hb1.setPadding(new Insets(20,20,20,20));
	VBox vb1 = new VBox();
	VBox vb0 = new VBox();

	Label search = new Label();
	search.setText("Search Songs: ");
	TextField searchField = new TextField();
	searchField.setPrefColumnCount(20);
	Button searchButton = new Button();
	searchButton.setText("search");

	

	
	searchButton.setOnAction(new EventHandler<ActionEvent>() {
		@Override public void handle(ActionEvent e) {
		    Song currSong = dafsa.search(searchField.getText());
		    if (currSong != null) {

			
			Button playlistAdd = new Button();
			playlistAdd.setText("Add to Playlist");

			
			playlistAdd.setOnAction(new EventHandler<ActionEvent>() {
				@Override public void handle(ActionEvent e) {
				    playlist.enqueue(currSong);
				}
			    });
			
			Label songName = new Label();
			songName.setText(currSong.title());
			Label songArtist = new Label();
			songArtist.setText(currSong.artist());
			Label songLength = new Label();
			int sLength = currSong.length();
			int mins = (int) sLength / 60;
			int seconds = sLength % 60;
			String lenString = Integer.toString(mins) + ":" + Integer.toString(seconds); 
			songLength.setText(lenString);
			vb1.getChildren().addAll(songName, songArtist, songLength);
			hb1.getChildren().addAll(vb1, playlistAdd);
		    } else {
			vb1.getChildren().clear();
			hb1.getChildren().clear();
		    }
		}
	    });

	HBox hb2 = new HBox();
	if (songTimer == 0) {
	    if (!playlist.isEmpty()) {
		currSongPlayed = playlist.dequeue();
		songTimer = currSongPlayed.length();
	    }
	}

	if (!playlist.isEmpty() || currSongPlayed != null) {
	    
	    Button btPlay = new Button();
	    if (paused == true) {
		btPlay.setText("Play");
	    } else {
		btPlay.setText("Pause");
	    }

	    Button btSkip = new Button();
	    btSkip.setText("Skip");

	    hb2.getChildren().addAll(btPlay, btSkip);
	}
	
	hb0.getChildren().add(search);
	hb0.getChildren().add(searchField);
	hb0.getChildren().add(searchButton);
	hb0.setSpacing(10);
	hb0.setPadding(new Insets(20,20,20,20));

	vb0.getChildren().add(hb0);
	vb0.getChildren().add(hb1);
	vb0.getChildren().add(hb2);
	

	Scene scene = new Scene(vb0, 600, 400);
	primaryStage.setTitle("Karaoke");
	primaryStage.setScene(scene);
	primaryStage.show();
    }

    public static Dafsa loadData() {
	Dafsa dafsa = new Dafsa(new Node());
	try {
	    Scanner sc = new Scanner(new File("songDataTest"));
	    int songCount = 0;
	    while (sc.hasNext()) {
		String songStr = sc.nextLine();
		String arr[] = songStr.split("\t");
		Song song = new Song(arr[0], arr[1], Integer.parseInt(arr[2]), arr[3]);
		songCount++;
		dafsa.add(song);
	    }
	} catch (IOException ioe) {
	    ioe.getMessage();
	}
	return dafsa;
    }
}
