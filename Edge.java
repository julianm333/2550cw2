

public class Edge {
    private Node src;
    private Node dest;
    private Song song;
    
    public Node src() {
        return src;
    }
    
    public Node dest() {
        return dest;
    }
    
    public Song song() {
        return song;
    }

    public void setSong(Song s) {
	song = s;
    }
    
    public Edge(Node s, Node d, Song sg) {
        src = s;
        dest = d;
        song = sg;
    }
}
