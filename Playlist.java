

public class Playlist {
    private plNode head;
    private plNode tail;
    public Playlist() {
        head = null;
    }
    public void enqueue(Song song) {
	plNode newNode = new plNode(song, null);
	if (head == null) {
	    head = newNode;
	    tail = newNode;
	} else {
	    tail.setNext(newNode);
	    tail = newNode;
	}
    }
    public Song dequeue() {
        if (head != null) {
            Song ret = head.song();
            head = head.next();
            return ret;
        }
        return null;
    }

    public Song[] getAll() {
	Song ret[] = new Song[];
	int index = 0;
	plNode temp = head;
	while (temp != null) {
	    ret[index] = temp.song();
	    temp = temp.next();
	}
	return ret;
    }

    public boolean isEmpty() {
	if (head != null) {
	    return false;
	}
	return true;
    }
}
