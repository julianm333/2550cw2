

public class Dafsa {
    private Node start;
    
    public Node start() {
        return start;
    }
    
    public Dafsa(Node n) {
        start = n;
    }
    
    public void add(Song song) {
	String songname = song.title();
        int index = 0;
        Node currNode = start;
        while (index < songname.length()) {
            Edge currEdge = currNode.charEdge(songname.charAt(index));
            if (currEdge != null) {
		if (index + 1 == songname.length()) {
		    currEdge.setSong(song);
		}
                currNode = currEdge.dest();
            } else {
                Node newNode = new Node();
                Song tempSong = null;
                if (index + 1 == songname.length()) {
                    tempSong = song;
                }
                Edge newEdge = new Edge(currNode, newNode, tempSong);
                currNode.addChar(songname.charAt(index), newEdge);
                currNode = newNode;
            }
            index++;
        }
    }

    public Song search(String word) {
	if (word.equals("")) {return null;}
	
        int index = 0;
        Node currNode = start;
        Edge currEdge = currNode.charEdge(word.charAt(index));
        while (index < word.length()) {
            currEdge = currNode.charEdge(word.charAt(index));
            if (currEdge != null) {
                currNode = currEdge.dest();
            } else {
                return null;
            }
            index++;
        }
        return currEdge.song();
    }
}
