

public class Song {
    private String title;
    private String artist;
    private int length;
    private String fileName;
    public String title() {
        return title;
    }
    public String artist() {
        return artist;
    }
    public int length() {
        return length;
    }
    public String fileName() {
        return fileName;
    }
    public Song(String t, String a, int l, String fn) {
        title = t;
        artist = a;
        length = l;
        fileName = fn;
    }
}