

public class plNode {
    private Song song;
    private plNode next;
    public plNode(Song s, plNode pln) {
        song = s;
        next = pln;
    }
    public Song song() {
        return song;
    }
    public plNode next() {
        return next;
    }
    public void setNext(plNode pln) {
        next = pln;
    }
}