import java.util.HashMap;

public class Node {
    private HashMap<Character, Edge> edgeHash;
    public Edge charEdge(Character c) {
        if (edgeHash != null) {
            return edgeHash.get(c);
        }
        return null;
    }
    public void addChar(Character c, Edge n) {
        edgeHash.put(c, n);
    }
    public void updateChar(Character c, Edge n) {
        edgeHash.replace(c, n);
    }
    public Node() {
        edgeHash = new HashMap<Character, Edge>();
    }
}